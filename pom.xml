<?xml version="1.0" encoding="UTF-8"?>
<!--

    Copyright (C) 2000 - 2017 Silverpeas

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    As a special exception to the terms and conditions of version 3.0 of
    the GPL, you may redistribute this Program in connection with Free/Libre
    Open Source Software ("FLOSS") applications as described in Silverpeas's
    FLOSS exception.  You should have received a copy of the text describing
    the FLOSS exception, and it is also available here:
    "http://www.silverpeas.com/legal/licensing"

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses />.

-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.silverpeas</groupId>
  <artifactId>silverpeas-test-dependencies-bom</artifactId>
  <version>1.0-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>Silverpeas Test Dependencies BOM</name>
  <description>A BOM with all the dependencies required to build and run the unit and the integration tests of Silverpeas</description>
  <url>http://www.silverpeas.org</url>

  <scm>
    <connection>scm:git:git@github.com:Silverpeas/silverpeas-test-dependencies-bom.git</connection>
    <developerConnection>scm:git:git@github.com:Silverpeas/silverpeas-test-dependencies-bom.git
    </developerConnection>
    <url>http://gitub.com/Silverpeas/silverpeas-test-dependencies-bom.git</url>
    <tag>HEAD</tag>
  </scm>

  <distributionManagement>
    <repository>
      <id>silverpeas</id>
      <name>Repository Silverpeas</name>
      <layout>default</layout>
      <url>http://www.silverpeas.org/nexus/content/repositories/releases/</url>
    </repository>
    <snapshotRepository>
      <id>silverpeas-snapshots</id>
      <name>Snapshots Repository Silverpeas</name>
      <layout>default</layout>
      <url>http://www.silverpeas.org/nexus/content/repositories/snapshots/</url>
    </snapshotRepository>
  </distributionManagement>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>${junit.version}</version>
        <scope>test</scope>
        <exclusions>
          <exclusion>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
      <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-core</artifactId>
        <version>${mokito.version}</version>
        <scope>test</scope>
        <exclusions>
          <exclusion>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-core</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
      <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest-all</artifactId>
        <version>1.3</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>com.ninja-squad</groupId>
        <artifactId>DbSetup</artifactId>
        <version>${dbsetup.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.awaitility</groupId>
        <artifactId>awaitility</artifactId>
        <version>3.0.0</version>
      </dependency>
      <!-- TODO to be removed later in profit of DbSetup -->
      <dependency>
        <groupId>org.dbunit</groupId>
        <artifactId>dbunit</artifactId>
        <version>2.5.1</version>
        <exclusions>
          <exclusion>
            <groupId>org.apache.poi</groupId>
            <artifactId>poi-ooxml</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
      <!-- end TODO -->
      <dependency>
      	<groupId>org.jboss.spec</groupId>
      	<artifactId>jboss-javaee-all-7.0</artifactId>
        <version>1.1.0.Final</version>
      	<scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.jboss.arquillian.junit</groupId>
        <artifactId>arquillian-junit-core</artifactId>
        <version>${arquillian.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.wildfly.arquillian</groupId>
        <artifactId>wildfly-arquillian-container-managed</artifactId>
        <version>${arquillian.wildfly.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.jboss.arquillian.core</groupId>
        <artifactId>arquillian-core-api</artifactId>
        <version>${arquillian.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.jboss.arquillian.junit</groupId>
        <artifactId>arquillian-junit-container</artifactId>
        <version>${arquillian.version}</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.jboss.shrinkwrap.resolver</groupId>
        <artifactId>shrinkwrap-resolver-impl-maven</artifactId>
        <version>3.0.0</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>com.carrotsearch</groupId>
        <artifactId>junit-benchmarks</artifactId>
        <version>0.7.2</version>
        <scope>test</scope>
      </dependency>
      <!-- LDAP test dependencies BEGIN -->
      <dependency>
        <groupId>org.forgerock.opendj</groupId>
        <artifactId>opendj-server</artifactId>
        <version>2.6.2</version>
        <scope>test</scope>
      </dependency>
      <dependency>
        <groupId>org.forgerock.commons</groupId>
        <artifactId>json-resource-servlet</artifactId>
        <version>2.4.2</version>
        <scope>test</scope>
        <exclusions>
          <exclusion>
            <groupId>org.codehaus.jackson</groupId>
            <artifactId>jackson-core-asl</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
      <dependency>
        <groupId>com.sleepycat</groupId>
        <artifactId>je</artifactId>
        <version>5.0.73</version>
        <scope>test</scope>
      </dependency>
      <!-- LDAP test dependencies END -->

      <!-- Mail -->
      <dependency>
        <groupId>com.icegreen</groupId>
        <artifactId>greenmail</artifactId>
        <version>1.5.5</version>
        <scope>test</scope>
      </dependency>

      <!-- resteasy-jaxrs API -->
      <dependency>
        <groupId>org.jboss.resteasy</groupId>
        <artifactId>resteasy-jaxrs</artifactId>
        <version>${resteasy.version}</version>
        <scope>test</scope>
      </dependency>

      <!-- CDI JUnit Tests-->
      <dependency>
        <groupId>org.jglue.cdi-unit</groupId>
        <artifactId>cdi-unit</artifactId>
        <version>4.0.1</version>
        <scope>test</scope>
      </dependency>

      <dependency>
        <groupId>org.jboss.weld.se</groupId>
        <artifactId>weld-se</artifactId>
        <version>2.4.5.Final</version>
        <scope>test</scope>
      </dependency>

      <!-- READ BEANS BY AN EASY WAY -->
      <dependency>
        <groupId>commons-beanutils</groupId>
        <artifactId>commons-beanutils</artifactId>
        <version>1.9.2</version>
        <scope>test</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <properties>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <junit.version>4.12</junit.version>
    <mokito.version>2.12.0</mokito.version>
    <dbsetup.version>2.1.0</dbsetup.version>
    <resteasy.version>3.0.11.Final</resteasy.version>
    <arquillian.version>1.1.14.Final</arquillian.version>
    <arquillian.wildfly.version>2.1.0.Final</arquillian.wildfly.version>
  </properties>

</project>
